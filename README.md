# Compressor that uses h5netcdf and hdf5plugin to compress files.

To use the compressor:

```python
from h5netcdfcompressor import transfer_file

compress_file(origin="original.nc", destination="compressed.nc", compression="lossy,sz,rel,1e-6")

```
