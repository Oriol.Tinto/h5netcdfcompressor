def load_plugins():
    """

    Function to print the instructions to set the environment variable HDF5_PLUGIN_PATH.

    """

    import hdf5plugin
    from pathlib import Path
    hdf5plugin_folder = Path(hdf5plugin.__file__).parent
    plugins_folder = hdf5plugin_folder / "plugins"
    print(f"export HDF5_PLUGIN_PATH={plugins_folder.as_posix()}")


def unload_plugins():
    print("unset HDF5_PLUGIN_PATH")

if __name__ == "__main__":
    import os
    if "HDF5_PLUGIN_PATH" in os.environ:
        load_plugins()
    else:
        unload_plugins()

