"""
Single access for different enstools-compressor utilities
"""

import hdf5plugin


# Few help messages
compressor_help_text = """
Few different examples

-Single file:
%(prog)s -o /path/to/destination/folder /path/to/file/1 

-Multiple files:
%(prog)s -o /path/to/destination/folder /path/to/file/1 /path/to/file/2

-Path pattern:
%(prog)s -o /path/to/destination/folder /path/to/multiple/files/* 

Launch a SLURM job for workers:
%(prog)s -o /path/to/destination/folder /path/to/multiple/files/* --nodes 4

To use custom compression parameters:
%(prog)s -o /path/to/destination/folder /path/to/multiple/files/* --compression compression_specification

Where compression_specification can contain the string that defines the compression options that will be used in the whole dataset,
        or a filepath to a configuration file in which we might have per variable specification.
        For lossless compression, we can choose the backend and the compression leven as follows
            "lossless:backend:compression_level(from 1 to 9)"
        The backend must be one of the following options:
                'blosclz'
                'lz4'
                'lz4hc'
                'snappy'
                'zlib'
                'zstd'
        For lossy compression, we can choose the compressor (wight now only zfp is implemented),
        the method and the method parameter (the accuracy, the precision or the rate).
        Some examples:
            "lossless"
            "lossy"
            "lossless,zlib,5"
            "lossy,zfp,accuracy,0.00001"
            "lossy,zfp,precision,12"
            
        If using a configuration file, the file should follow a json format and can contain per-variable values.
        It is also possible to define a default option. For example:
        { "default": "lossless",
          "temp": "lossy,zfp,accuracy,0.1",
          "qv": "lossy,zfp,accuracy,0.00001"        
        }


So, few examples with custom compression would be:

%(prog)s -o /path/to/destination/folder /path/to/multiple/files/* --compression lossless

%(prog)s -o /path/to/destination/folder /path/to/multiple/files/* --compression lossless,blosclz,9

%(prog)s -o /path/to/destination/folder /path/to/multiple/files/* --compression lossy

%(prog)s -o /path/to/destination/folder /path/to/multiple/files/* --compression lossy,zfp,rate,4

%(prog)s -o /path/to/destination/folder /path/to/multiple/files/* --compression compression_paramters.json


Last but not least, now it is possible to automatically find which are the compression parameters that must be applied
to each variable in order to maintain a 0.99999 Pearson correlation

%(prog)s -o /path/to/destination/folder /path/to/multiple/files/* --compression auto


"""


# For each possible usage (compress, analyze, ...) we will define a function to add
# the corresponding command line arguments to the parser and another one to manage the call

###############################
# Compressor
def add_subparser_compressor(parser):
    parser.add_argument("-i", '--input', type=str,
                        dest="input", default=None, required=True)
    parser.add_argument("-o", '--output', type=str,
                        dest="output", default=None, required=True)

    parser.add_argument("-p", '--parts', type=int,
                        dest="parts", default=-1,
                        help="Perform copy operations in parts. If use -1 to do time-step by time-step and 1 to perform the copy in one time, use n to split the copy in an n number of parts.")

    parser.add_argument('--compression', type=str, dest="compression", nargs="+", default="lossless",
                        help="""
        Specifications about the compression options. Default is: %(default)s""")
    parser.add_argument("--variables", dest="variables", default=None, type=str,
                        help="List of variables to be kept. The other variables will be dropped."
                        "Must be a list of comma separated values: i.e. vor,temp,qv"
                        "Default=None")


def call_compressor(args):
    from os.path import realpath
    # Read the output folder from the command line and assert that it exists and has write permissions.
    output = realpath(args.output)

    _input = realpath(args.input)

    parts = args.parts

    # Compression options
    compression = args.compression

    if isinstance(compression, list):
        compression = " ".join(compression)

    if compression in ["None", "none"]:
        compression = None

    # List of variables
    variables = args.variables
    if variables is not None:
        variables = variables.split(",")

    # Import and launch compress function
    from h5netcdfcompressor import compress_file
    compress_file(_input, output, compression,variables_to_keep=variables, parts=parts)


def expand_paths(string: str):
    import glob
    from os.path import realpath
    """
    Small function to expand the file paths
    """
    files = glob.glob(string)
    return [realpath(f) for f in files]


###############################

def main():
    # Create parser
    import argparse

    # Create the top-level parser
    parser = argparse.ArgumentParser()
    # Add the different subparsers.
    # If willing to add new parser, this is the function where to look at.
    add_subparser_compressor(parser)
    # Parse the command line arguments
    args = parser.parse_args()
    print(args)
    call_compressor(args)

