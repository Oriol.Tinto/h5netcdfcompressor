from pathlib import Path
from typing import List


def compress_file(origin: Path,
                  destination: Path,
                  compression: str,
                  variables_to_keep: List[str] = None,
                  parts=-1,
                  **kwargs):
    if kwargs:
        print(f"The h5netcdf implementation of transfer_file ignores the following keyword arguments: {kwargs}")
    import h5netcdf
    from enstools.encoding.api import FilterEncodingForXarray

    # Still using xarray to get the encoding
    import xarray as xr
    with xr.open_dataset(origin, engine="h5netcdf") as ds:
        # Fet variables
        data_vars = variables_to_keep if variables_to_keep is not None else [v for v in ds.data_vars]
        encoding = FilterEncodingForXarray(ds, compression=compression)

    # Open the origin and destination files.
    with h5netcdf.File(origin, 'r') as source_file, h5netcdf.File(destination, 'w') as destination_file:
        # Copy global attributes
        for attr in source_file.attrs:
            destination_file.attrs[attr] = source_file.attrs[attr]

        # Add dimensions
        for dimension_name in source_file.dimensions:
            dimension = source_file.dimensions[dimension_name]
            if not dimension.isunlimited():
                destination_file.dimensions[dimension.name] = dimension.size
            else:
                destination_file.dimensions[dimension.name] = None
                destination_file.resize_dimension(dimension.name, dimension.size)

        # Get the list of variables to copy which include the data variables and the coordinates with values
        variables_to_copy = data_vars + [v for v in source_file.variables if v in source_file.dimensions]

        # Copy variable by variable
        for var_name in variables_to_copy:
            source_var = source_file[var_name]
            var_encoding = {**encoding[var_name]}
            # xarray and h5netcdf use different keyword for the chunks. Here we replace the key
            if "chunksizes" in var_encoding:
                var_encoding["chunks"] = var_encoding.pop("chunksizes")
            # Create variable without the data
            destination_file.create_variable(name=var_name,
                                             dimensions=source_var.dimensions,
                                             dtype=source_var.dtype,
                                             **var_encoding,
                                             )

            # Get the destination variable object
            destination_var = destination_file[var_name]

            # Copy values.

            # If the variable has a time dimension,
            if "time" in destination_var.dimensions:
                times = source_file["time"][()]
                # Slicing
                indices = list(range(len(times)))
                if parts == -1:
                    parts = len(times)
                slices = slices_from_list(indices, parts=parts)
                for idx, slc in enumerate(slices, start=1):
                    data_to_copy = source_var[slc][()]
                    destination_var[slc] = data_to_copy
            else:
                destination_var[:] = source_var[()]

            # Copy variable attributes
            for attr in source_var.attrs:
                destination_var.attrs[attr] = source_var.attrs[attr]


def split_list(input_list, parts, round_robin=False):
    """
    Split list in parts of a similar size
    :param input_list: list
    :param parts: integer
    :param round_robin: boolean
    :return:
    """
    # Some checks
    assert parts > 0, "Number of parts should be bigger than 0!"

    # If input is a numpy array, convert it to list
    if not isinstance(input_list, list):
        input_list = list(input_list)

    # Size of the output lists
    output_list_size = int(len(input_list) / parts)

    # Residual
    residual = len(input_list) % parts

    # Create empty output lists
    output_lists = [[] for i in range(parts)]

    if round_robin is False:
        # Put the elements of each part
        for i in range(parts):
            for j in range(output_list_size):
                output_lists[i].append(input_list.pop(0))
            if i < residual:
                output_lists[i].append(input_list.pop(0))
    else:
        while len(input_list) > 0:
            for i in range(parts):
                try:
                    output_lists[i].append(input_list.pop(0))
                except IndexError:
                    break

    return output_lists


def from_list_to_slice(_list) -> slice:
    """
    Convert a list of elements to a slice.
    :param _list:
    :return:
    """
    return slice(min(_list), max(_list) + 1)


def slices_from_list(input_list, parts):
    """
    Divide a list into several slices.
    :param input_list:
    :param parts:
    :return:
    """
    sub_lists = split_list(input_list, parts)
    return [from_list_to_slice(_sl) for _sl in sub_lists]

